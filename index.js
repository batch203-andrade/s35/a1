const mongoose = require('mongoose');
const express = require('express');
const app = express();
const port = 5500;

require('dotenv').config();
const db_url = process.env.DB_URL;

app.use(express.json()); // Allow app to read JSON data
app.use(express.urlencoded({ extended: true })); // Allow to read other data types from forms

mongoose.connect(db_url, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Successfully Connected to MongoDB Atlas"));

// IMPORT MODEL 
const UserModel = require('./UserModel');

// SIGNUP HERE POST METHOD
app.post('/signup', (req, res) => {
    UserModel.find({}, (err, result) => {
        let truth = result.some(e => e.username == req.body.username);
        if (!truth) {
            let newUser = new UserModel({
                "username": req.body.username,
                "password": req.body.password
            });

            newUser.save((saveErr, saveUser) => {
                if (err) throw res.send(saveErr);
                else return res.status(200).send("USER HAS BEEN SAVED");
            });

        }
        else return res.send("There is a similar username in the database. Please choose another username.");
    })
})

app.listen(port, () => console.log(`Server running now at port ${port}`));
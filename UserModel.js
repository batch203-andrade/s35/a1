const mongoose = require('mongoose');

//Schema User Here
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username is required"]
    },
    password: {
        type: String,
        required: [true, "Username is required"]
    }
});

const UserModel = mongoose.model("User", userSchema);

module.exports = UserModel;